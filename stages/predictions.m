function ygamma = predictions(t,hp,Xway,Xwaydot)

    ygamma = zeros(hp*6,1);
    for iter=t+1:t+hp        
        ygamma((iter-(t+1))*6+1:((iter-(t+1))*6+6))  = [Xway(:,iter);Xwaydot(:,iter)];
    end


end

