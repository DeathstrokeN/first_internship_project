function t = InvDyn(A, Bp, X, Xdot, X2dot, m, g, I)


[W, u, v] = Wrenchmatrix(A,Bp,X); %-J'
[M, G] = MGfun(m, g, I);

%Mx2 + G = Wt
% W*t - M*X2dot - G = 0
% W*t = M*X2dot + G
Aeq = W;
beq = M*X2dot + G;
% Aeq * t = beq;
t = fmincon(@(t) criterion(t), zeros(4,1), [],[],Aeq,beq,20*ones(4,1),200*ones(4,1));
% x = fmincon(fun,x0,A,b,Aeq,beq,lb,ub)
%Must return vector of tension force
end

