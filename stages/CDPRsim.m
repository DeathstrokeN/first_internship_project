%%...
clearvars;
close all
%timeVars
ts = 0.01; %1ms
tf = 10; %10sec
g = 9.8;
m = 2; %g
I = 1;
%Position
xi=.5; yi=.5;
Phi=0;%30 degrees, Orientation of the end effector
Xi = [xi yi Phi]';
Xf = [.7 .4 pi/6]';
Xdot = [1 2 3]';
X2dot = [1 1 1]';


%%
%IKM
%[A, B, Lengths, Angles]=draw(xi,yi,Phi)
Ap       = [ 0   0   1   1 ;
             0   1   1	0];
Bp      = [ -.10  -.10  .10   .10 ;
             .10  -.10 -.10   .10];
B = MGI(Xi,Ap,Bp);

draw(Ap,B,xi,yi)
%%
%IDKM
tensions = InvDyn(Ap, Bp, Xi, Xdot, X2dot, m, g, I);

%%
%Trajectory generation
Xway = TrajGen(tf, ts, Xi, Xf);
N = round(tf/ts);
t=(0:ts:(tf-ts));
ref = Xf*ones(1,N);
figure('Name','Trajectory generation');
subplot(311);
plot(t,Xway(1,:),'k',t,ref(1,:),'r')
legend('xd','xf')
xlabel('Time (s)')
ylabel('x(m)')
title('Trajectory')
%axis equal
grid
subplot(312);
plot(t,Xway(2,:),'k',t,ref(2,:),'r')
legend('yd','yf')
xlabel('Time (s)')
ylabel('y(m)')
%axis([0 N 0 1])
grid
subplot(313);
plot(t,Xway(3,:),'k',t,ref(3,:),'r')
legend('Phid','Phif')
xlabel('Time (s)')
ylabel('Phi(rad)')
%axis([0 N -pi pi])
grid

%%
% Design of a control LOOP
% matrix to save the data (t and X(output))
simdata_t = zeros(4,N);
simdata_X = zeros(3,N);
%disturbance
f_ext = [zeros(3,300) [10;10;3]*ones(1,200) zeros(3,N-500)]; % Applied at t = 3s for 2sec
%Constant matrices
[M, G] = MGfun(m, g, I);

%%
xdotpast = zeros(3,1);
xdot = zeros(3,1);
error_intpast = zeros(3,1);
error_int = zeros(3,1);
Xpast = Xi;
error = zeros(3,1);
errorpast = zeros(3,1);
% Simulation LOOP :
t_vec = linspace(0,tf,N);
simdata_X(:,1) = Xpast;
Xwaydot = nan(3,N);
Xway2dot = nan(3,N);
for t=1:N-1
    Xwaydot(:,t) = (Xway(:,t+1) - Xway(:,t))/ts;
end
for t=1:N-1
    Xway2dot(:,t) = (Xwaydot(:,t+1) - Xwaydot(:,t))/ts;
end

%PID tuning
P       = 1e1*[1e2       0        0;
               0         1e2      0;
               0         0        1e2];
 
D      =  1e3*[1e1     0       0;
                 0     1e1     0;
                 0     0       1e1];
  
I     =  1e1*[1   0        0;
              0     1      0;
              0     0      1];
  
ttpast = tensions;

for t=2:N-2
    ff          = M*Xway2dot(:,t) - .8*G;
    fd          = P * error + D * (error - errorpast) + I * error_int + ff; %Controller "force desired"
    [W, u, v]   = Wrenchmatrix(Ap,Bp,Xpast); %-J' calculate Wrench matrix for every pose
    %fd         = W*tt;
    H           = 2*eye(4);
    f           = zeros(1,4);
    Aeq         = W;
    beq         = fd;
    tt          = quadprog(H,f,[],[],Aeq,beq,20*ones(4,1),200*ones(4,1), ttpast); %Cable tensions
    ttpast      = tt;
    %Plant (ode45 equations)
    x2dot       = inv(M)*(W*tt + f_ext(:,t) + G);  % to be replaced : 0 by f_ext(t,:)'
    %trying to split every line in ode45
    tspan       = [t_vec(t-1) t_vec(t)];
    f           = @(t,y) [y(4:6);x2dot];
    %f2          = @(t,X) X(:,1);
    ypast       = [Xpast; xdot];
    [~,y]       = ode45(f,tspan,ypast);
    y           = y(end,:);
    X           = y(1:3)';
    xdot        = y(4:6)';    
    errorpast   = error;
    error       = Xway(:,t) - X; %error
    error_int   = error + error_int;
    
    %storing data
    Xpast       = X; %position
    simdata_t(:,t-1) = ttpast;%cable tensions
    simdata_X(:,t) = X;
    %Animation using draw
    B = MGI(X,Ap,Bp);
    if mod(t,10) == 0
        draw(Ap,B,X(1),X(2))
    end
    drawnow
    t
    ttpast = tt;
end
   

simdata_t(:,N-2) = ttpast;
%%
%plotting output variable
    
N = round(tf/ts)-2;
t=(0:ts:(tf-3*ts));
figure('Name','system output');
subplot(311);
plot(t,Xway(1,1:N),'k',t,simdata_X(1,1:N),'r')
legend('xway','x')
xlabel('Time (s)')
ylabel('x(m)')
title('Pose')
%axis([0 N 0 1])
grid
subplot(312);
plot(t,Xway(2,1:N),'k',t,simdata_X(2,1:N),'r')
legend('yway','y')
xlabel('Time (s)')
ylabel('y(m)')
%axis([0 N 0 1])
grid
subplot(313);
plot(t,Xway(3,1:N),'k',t,simdata_X(3,1:N),'r')
legend('phiway','phi')
xlabel('Time (s)')
ylabel('Phi(rad)')
%axis([0 N -pi pi])
grid


%%
%plotting system input variables
figure('Name','controller output');
plot(t,simdata_t(1,1:N),t,simdata_t(2,1:N),t,simdata_t(3,1:N),t,simdata_t(4,1:N),t,20*ones(N,1),'.',t,200*ones(N,1),'.')
ylabel('Cable tension')
xlabel('Time (s)')
title('Input variables')
legend('t1','t2','t3','t4','Lower limit','Upper limit')
axis([0 tf 0 220])
grid



%%
% plotting error
%plotting output variable
    

figure('Name','output error');
subplot(311);
plot(t,Xway(1,1:N) - simdata_X(1,1:N),'r')
legend('xway','x')
xlabel('Time (s)')
ylabel('error on x(m)')
title('Error MPC')

%axis([0 N 0 1])
grid
subplot(312);
plot(t,Xway(2,1:N) - simdata_X(2,1:N),'r')
xlabel('Time (s)')
ylabel('error on y(m)')
%axis([0 N 0 1])
grid
subplot(313);
plot(t,Xway(3,1:N) - simdata_X(3,1:N),'r')
xlabel('Time (s)')
ylabel('error on Phi(rad)')
%axis([0 N -pi pi])
grid
