function [M, G] = MGfun(m, g, I)

%M
M = [m*eye(2) zeros(2,1);
     0        0       I];
 
G = [0;
    -m*g;
    0];


end

