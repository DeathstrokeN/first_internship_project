function RotMatrix = Rot(Phi)

%RotMatrix = [1 0 0;0 cos(Phi) 0; 0 0 1];
RotMatrix = [cos(Phi) -sin(Phi);
             sin(Phi) cos(Phi)];

end