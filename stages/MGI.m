function B = MGI(X,A,Bp)

x_R = X(1);
y_R = X(2);
Phi = X(3);

B = Rot(Phi)*Bp + [x_R; y_R];

%Cables lenghts
% L1 = sqrt((B(1,1) - A(1,1))^2 + (B(2,1) - A(2,1))^2);
% L2 = sqrt((B(1,2) - A(1,2))^2 + (B(2,2) - A(2,2))^2);
% L3 = sqrt((B(1,3) - A(1,3))^2+(B(2,3) - A(2,3))^2);
% L4 = sqrt((B(1,4) - A(1,4))^2+ (B(2,4) - A(2,4))^2);
% 
% Lengths = [L1; L2; L3; L4];



end

