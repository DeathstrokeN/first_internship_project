function r = polynomr(tf, t)

r = 10*(t/tf)^3 - 15*(t/tf)^4 + 6*(t/tf)^5; 

end

