function draw(A, B, xi, yi)
%Inputs : cables points coordiantes
%Robot dimensions 1m*1m
%end effector 0.1m*0.1m


%limits
clf
xlim = 100;
ylim = 100;
%figure('Name','Planar CDPR Simulation')
%Labels
xlabel('x (width)')
ylabel('y (height)')
title('Simulation')
grid
hold on

%Position of the robot
% x_R = x;
% y_R = y;
%%drawing initial position position
% if x_R>xlim || x_R<0 || y_R>ylim || y_R<0
%     fprintf('NOT FAISIBLE');
%     msgbox('Point hors repère')
% else
    %Cable 1
    plot([A(1,1) B(1,1)],[A(2,1) B(2,1)]);
    %Cable 2
    plot([A(1,2) B(1,2)],[A(2,2) B(2,2)]);
    %Cable 3
    plot([A(1,3) B(1,3)],[A(2,3) B(2,3)]);
    %Cable 4
    plot([A(1,4) B(1,4)],[A(2,4) B(2,4)]);
    %Robot platform (end effector)
    xe = [B(1,1), B(1,2), B(1,3), B(1,4), B(1,1)];
    ye = [B(2,1), B(2,2), B(2,3), B(2,4), B(2,1)];
    plot(xe,ye,'m')
    plot(xi,yi,'*')
    axis equal

% end



    


%%
% %Cables lenghts
% L1 = sqrt((x1-C1x)^2+(y1-C1y));
% L2 = sqrt((x2-C2x)^2+(y2-C2y));
% L3 = sqrt((x3-C3x)^2+(y3-C3y));
% L4 = sqrt((x4-C4x)^2+(y4-C4y));
% 
% Lengths = [L1; L2; L3; L4];
% 
% %%
% %Angles
% theta1 = atan((y1-C1y)/(x1-C1x));
% theta2 = atan((y2-C2y)/(x2-C2x));%+pi/2; %rotation of frame pi/2
% theta3 = atan((y3-C3y)/(x3-C3x));%+pi;
% theta4 = atan((y4-C4y)/(x4-C4x));%+3*pi/2;
% 
% Angles = rad2deg([theta1; theta2; theta3; theta4]);
% 
% A = [x1 x2 x3 x4; y1 y2 y3 y4; Angles(1) Angles(2) Angles(3) Angles(4)];%3*4
% B = [C1x C2x C3x C4x; C1y C2y C3y C4y];%

end

