
%%
%Initialization
%timeVars
% ts = 0.01; %1ms
% tf = 10; %10sec
% g = 9.8;
% m = 2; %g
% I = 1;
% %Position
% xi=.5; yi=.5;
% Phi=0;%30 degrees, Orientation of the end effector
% Xi = [xi yi Phi]';
% Xf = [.7 .4 pi/6]';
% Xdot = [1 2 3]';
% X2dot = [1 1 1]';




%%
 %y(k+1) = A*yk + B*tk + vk
 A = [eye(3)     ts*eye(3);
     zeros(3,3) eye(3)];

 B = [zeros(3,4);
     ts*(inv(M)*W)];
 
 v = [zeros(3,1);
      ts*(inv(M)*G)];

  
tt_k = ttpast;
ypred = zeros(6*hp,1);
ypred(1:6,1) = A*y + B*tt_k + v; 

tt_km1 = zeros(4,1);
tt = zeros(4*hc,1);
tt(1:4,1) = ttpast;

for i=2:hp
    if i<=hc
        tt_k1 = tt_k - tt_km1;
        tt((i-1)*4+1 : i*4 , 1) = tt_k1;
    else
        tt_k1 = tt_k;
    end
        
    ypred((i-1)*6+1 : i*6 , 1) = A*ypred((i-2)*6+1:(i-1)*6 , 1) + B*tt_k1 + v;
    
    tt_km1 = tt_k;
    tt_k   = tt_k1;
end

ypred_matrices = E*tt+V+D*y;

Check = abs(ypred - ypred_matrices) < 1e-3;

[ypred, ypred_matrices, Check]