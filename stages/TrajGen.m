function Xway = TrajGen(tf, ts, Xi, Xf)
%TRAJGEN Summary of this function goes here
%   Detailed explanation goes here


N = round(tf/ts);%number of simulation points tf/ts, 20sec
Xway = nan(3,N);

for t=1:N
    r = polynomr(tf, t*ts);
    Xway(:,t) = Xi + r*eye(3)*(Xf-Xi);
end

end

