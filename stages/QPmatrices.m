function [D,E,V,Q,z] = QPmatrices(tt,ts,W,M,G,hp,hc)

 %y(k+1) = A*yk + B*tk + vk
 A = [eye(3)     ts*eye(3);
     zeros(3,3) eye(3)];%6x6

 B = [zeros(3,4);
     ts*(inv(M)*W)];%6x4
 
 v = [zeros(3,1);
      ts*(inv(M)*G)];%6x1
  
  
 %matrix D
 D = zeros(hp*6,6);
 for i=1:hp
     D((i-1)*6+1:i*6 , :) = A^i;
 end
  

 
%   %for hp = hc
%   %matrix E
%    E = zeros(6*hp,4*hc);
%    for j = 1:hc
%        for i=j:hp
%            E((i-1)*6+1:i*6 , (j-1)*4+1:j*4) = A^(i-j)*B;
%        end
%    end
%   
 
 
 
 %for hp != hc
 %matrix E to be reviewed
 E = zeros(6*hp,4*hc);
 for j = 1:hc
     for i=1:hp
         e  = zeros(6,4);
         if (j == hc)&&(j >= i)
             for k = 1:i-hc+1
                 e = e + A^(k-1)*B;
             end
             E((i-1)*6+1:i*6 , (hc-1)*4 + 1 : hc*4) = e;
         else
             E((i-1)*6+1:i*6 , (j-1)*4+1:j*4) = A^(i-j)*B;

         end
     end
 end
     
 
 %matrix v
 V = zeros(6*hp,1);
 for i=1:hp
     V((i-1)*6+1:i*6 , :) = A^(i-1)*v;
 end
  
 %matrix u
%  u      = zeros((hc+1)*4);
%  u(1:4) = t;
%  for i=2:hc+1
%      u((i-1)*4+1:i*4) = t((i-1)*4+1:i*4);
%  end
 
 %matrix Q
 Q = eye(4*hc);
 for i=2:hc
     Q((i-1)*4+1:i*4 , (i-2)*4+1:(i-1)*4) = -eye(4);
 end
 
 %matrix z
 z = zeros(4*hc,1);
 z(1:4,1) = tt;


end

