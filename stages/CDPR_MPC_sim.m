%%...
clearvars;
close all
%timeVars
ts = 0.01; %1ms
tf = 10; %10sec
g = 9.8;
m = 2; %kg
I = 0.2;
%Position
xi=.5; yi=.5;
Phi=0;%30 degrees, Orientation of the end effector
Xi = [xi yi Phi]';
Xf = [.7 0.4 pi/6]';
Xdot = [1 2 3]';
X2dot = [1 1 1]';
high = 200;
low  = 20;


%%
%IKM
%[A, B, Lengths, Angles]=draw(xi,yi,Phi)
Ap       = [ 0   0   1   1 ;
             0   1   1	0];
Bp      = [ -.10  -.10  .10   .10 ;
             .10  -.10 -.10   .10];
B = MGI(Xi,Ap,Bp);

draw(Ap,B,xi,yi)
%%
%IDKM
tensions = InvDyn(Ap, Bp, Xi, Xdot, X2dot, m, g, I);
[W, u, v] = Wrenchmatrix(Ap,Bp,Xi); %-J'
%%
%Trajectory generation
Xway = TrajGen(tf, ts, Xi, Xf);
N = round(tf/ts);
t=(0:N-1);
% ref = Xf*ones(1,N);
% figure('Name','Trajectory generation');
% subplot(311);
% plot(t,Xway(1,:),'k',t,ref(1,:),'r')
% legend('output signal x','desired final xf')
% xlabel('temps (ms)')
% ylabel('x(m)')
% axis([0 N 0 1])
% grid
% subplot(312);
% plot(t,Xway(2,:),'k',t,ref(2,:),'r')
% legend('output signal y','desired final yf')
% xlabel('temps (ms)')
% ylabel('y(m)')
% axis([0 N 0 1])
% grid
% subplot(313);
% plot(t,Xway(3,:),'k',t,ref(3,:),'r')
% legend('output signal Phi','desired final Phif')
% xlabel('temps (ms)')
% ylabel('Phi(rad)')
% axis([0 N -pi pi])
% grid

%%
% Design of a control LOOP
% matrix to save the data (t and X(output))
simdata_t = zeros(4,N);
simdata_X = zeros(3,N);
%disturbance
f_ext = [zeros(3,300) [10;-10;3]*ones(1,200) zeros(3,N-500)]; % Applied at t = 3s for 2sec
%Constant matrices
[M, G] = MGfun(m, g, I);

%%
xdotpast = zeros(3,1);
xdot = zeros(3,1);

Xpast = Xi;
error = zeros(3,1);
errorpast = zeros(3,1);

Xwaydot = nan(3,N);
for t=1:N-1
    Xwaydot(:,t) = (Xway(:,t+1) - Xway(:,t))/ts;
end

% Simulation LOOP :
t_vec = linspace(0,tf,N);
ttpast         = tensions;
simdata_X(:,1) = Xpast;
simdata_t(:,1) = ttpast;

x2dotpast = zeros(3,1);

hp = 20;
hc = 5;
y  = [Xi;
     xdotpast];
upast = [ttpast;zeros((hc-1)*4,1)];
%weights
Ku     = 1e-2*eye(4*hc);
%kgamma = 5e12*eye(6*hp);
kgamma_p = 5e12;
kgamma_v = 1e12;
kgamma = diag( repmat( [kgamma_p*ones(1,3) kgamma_v*ones(1,3)] , 1, hp ) );
% kgamma = [5e12*eye(3*hp)  0*eye(3*hp);
%           0*eye(3*hp)  1e8*eye(3*hp)];
KD     = 1e-2*eye(4*hc);

%disturbance
f_ext = [zeros(3,300) [10;10;3]*ones(1,200) zeros(3,N-500)]; % Applied at t = 3s for 2sec
simdata_X(:,1) =  Xi;
simdata_t(:,1) = ttpast;
% simdata_X(:,2) =  Xi;
% simdata_t(:,2) = ttpast;
for t=2:N-hp-1
    t
    [W, u, v]   = Wrenchmatrix(Ap,Bp,Xpast);%J-1
    %optimization
    %MPC_criterion(tt,ttpast,y,Xway,ts,W,M,G,hp,hc);
    [D,E,V,Q,z] = QPmatrices(ttpast,ts,W,M,G,hp,hc);
    ygammad     = predictions(t,hp,Xway,Xwaydot);
    H           = E'*kgamma*E + Ku + Q'*KD*Q;
    f           = (D*y - ygammad)'*kgamma*E - z'*KD*Q;
    u           = quadprog(H,f,[],[],[],[],low*ones(4*hc,1),high*ones(4*hc,1), upast); %optimisation
    tt          = u(1:4);%taking u(k), current time input
    %plant
    x2dot       = inv(M)*(W*tt + f_ext(:,t) + G); %model!
    tspan       = [t_vec(t-1) t_vec(t)];
    f           = @(t,y) [y(4:6);x2dot];
    ypast       = [Xpast; xdotpast];
    [~,y]       = ode45(f,tspan,ypast);
    y           = y(end,:)';
    X           = y(1:3);
    Xpast       = X;
    xdotpast    = y(4:6);
%     xdotpast = xdot;
%     X        = xdot + Xpast;
%     Xpast    = X;
%     y        = [X;
%                 xdot];
           
    %Animation using draw
    B = MGI(X,Ap,Bp);
    if mod(t,5) == 0
        draw(Ap,B,X(1),X(2))
    end
    drawnow
    t
    simdata_X(:,t) =  X;
    simdata_t(:,t) = tt;
    upast            =  u;
end
   

%%
%plotting output variable
    
N = round(tf/ts)-hp-1;
t=(0:ts:tf-(hp+2)*ts);
figure('Name','system output');
subplot(311);
plot(t,Xway(1,1:N),'k',t,simdata_X(1,1:N),'r')
legend('xway','x')
xlabel('Time (s)')
ylabel('x(m)')
title('Pose')
%axis([0 N 0 1])
grid
subplot(312);
plot(t,Xway(2,1:N),'k',t,simdata_X(2,1:N),'r')
legend('yway','y')
xlabel('Time (s)')
ylabel('y(m)')
%axis([0 N 0 1])
grid
subplot(313);
plot(t,Xway(3,1:N),'k',t,simdata_X(3,1:N),'r')
legend('phiway','phi')
xlabel('Time (s)')
ylabel('Phi(rad)')
%axis([0 N -pi pi])
grid


%%
%plotting system input variables
figure('Name','controller output');
plot(t,simdata_t(1,1:N),t,simdata_t(2,1:N),t,simdata_t(3,1:N),t,simdata_t(4,1:N),t,low*ones(N,1),'.',t,high*ones(N,1),'.')
ylabel('Cable tension')
xlabel('Time (s)')
title('Input variables')
legend('t1','t2','t3','t4','Lower limit','Upper limit')
axis([0 tf 0 high+low])
grid



%%
% plotting error
%plotting output variable
    

figure('Name','output error');
subplot(311);
plot(t,Xway(1,1:N) - simdata_X(1,1:N),'r')
xlabel('Time (s)')
ylabel('error on x(m)')
title('Error MPC')
grid
subplot(312);
plot(t,Xway(2,1:N) - simdata_X(2,1:N),'r')
xlabel('Time (s)')
ylabel('error on y(m)')
%axis([0 N 0 1])
grid
subplot(313);
plot(t,Xway(3,1:N) - simdata_X(3,1:N),'r')
xlabel('Time (s)')
ylabel('error on Phi(rad)')
%axis([0 N -pi pi])
grid
%textduo