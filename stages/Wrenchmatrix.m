function [W, u, v] = Wrenchmatrix(A,Bp,X)

x_R = X(1);
y_R = X(2);
Phi = X(3);

B = Rot(Phi)*Bp + [x_R; y_R];

%u
up      = [A - B];
u       = nan(2,4);
for i = 1:4
    u(:,i) = up(:,i)/norm(up(:,i));
end
%v
v = nan(2,4);
for i=1:4
    v(:,i) = B(:,i);
end


W = nan(3,4);

for i = 1:4   
    W(:,i)  = [u(:,i); u(1,i)*v(2,i)-u(2,i)*v(1,i)];
end




end

